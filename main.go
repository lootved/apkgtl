package main

import (
	"bufio"
	"encoding/xml"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

var tmp_dir string = "/tmp"
var pkg_id string = "com/pgk/toupdt"

func main() {
	wrkdir := tmp_dir + "/apk/fr/app-debug"

	incremental := false
	patchsDir := "patch"
	flag.StringVar(&wrkdir, "work", wrkdir, "folder where apktool output is located")
	flag.StringVar(&patchsDir, "patch", patchsDir, "folder containing custom smali to inject")
	flag.BoolVar(&incremental, "u", incremental, "if provided will only redeploy the custom code not the deps")
	flag.Parse()

	n := getSmaliClassNbr(wrkdir) + 1

	if incremental {
		dst := wrkdir + "/smali_classes" + strconv.Itoa(n-1)
		cmd("rm -rf " + dst)
		cmd("cp -r " + patchsDir + "/cust/1L/ " + dst)
		return
	}

	copySmaliPatch(patchsDir+"/deps", wrkdir, &n)
	copySmaliPatch(patchsDir+"/cust", wrkdir, &n)

	inputPath := wrkdir
	//inputPath += "/smali_classes3/sand/box/MainActivity.smali"
	inputPath += "/smali_classes3/" + pkg_id + "/AppController.smali"

	// "patch/MainActivity.smali"
	patchEntryPoint(inputPath)

}

func patchAndroidManifest() {
	// must add to <application properties
	//android:debuggable="true" android:extractNativeLibs="false"
}

func copySmaliPatch(path string, dst string, n *int) {
	f, err := os.ReadDir(path)
	check(err)

	for _, p := range f {
		if !p.IsDir() {
			continue
		}
		smaliDst := dst + "/smali_classes" + strconv.Itoa(*n)
		*n++
		cmd("cp -r " + path + "/" + p.Name() + " " + smaliDst)
	}
}

func getSmaliClassNbr(path string) int {
	f, err := os.ReadDir(path)
	check(err)
	nbr := 1
	for _, p := range f {
		if p.IsDir() && strings.HasPrefix(p.Name(), "smali_classes") {
			nbr++
		}
	}
	return nbr
}

func cmd(s string) {
	args := strings.Split(s, " ")
	prog := args[0]
	args = args[1:]
	fmt.Println("running:"+prog, args)
	c := exec.Command(prog, args...)
	check(c.Run())
}

type State int

const (
	lookingForOnCreate State = iota
	foundOnCreate
	onLocalLine
	onParamLine
	mustAddPath
	doNothing
)

func patchEntryPoint(path string) {
	stb := strings.Builder{}
	f, err := os.Open(path)
	check(err)
	sc := bufio.NewScanner(f)

	sc.Split(bufio.ScanLines)
	onCreateMethod := ".method protected onCreate(Landroid/os/Bundle;)V"
	onCreateMethod2 := ".method public final onCreate()V"

	patchLine1 := "sget-object v0, Ldbg/roid/Init;->INSTANCE:Ldbg/roid/Init;"
	patchLine2 := "invoke-virtual {v0}, Ldbg/roid/Init;->init()V"

	state := lookingForOnCreate
	for sc.Scan() {
		line := sc.Text()

		switch state {
		case lookingForOnCreate:
			if line == onCreateMethod || line == onCreateMethod2 {
				state = foundOnCreate
			}
		case foundOnCreate:
			state = onLocalLine

		case onLocalLine:
			state = onParamLine

		case onParamLine:
			state = mustAddPath

		case mustAddPath:
			{
				// file was previously patched no need to do anything
				if line == patchLine1 {
					f.Close()
					fmt.Println("Patch was previously applied, no need to do it again")
					return
				}
				// patch the file otherwise
				stb.WriteString(patchLine1)
				stb.WriteString("\n")
				stb.WriteString(patchLine2)
				stb.WriteString("\n")
				state = doNothing
			}
		case doNothing:
			{
				// nothing to do
			}

		}
		// add the line to the string builde
		stb.WriteString(line)
		stb.WriteString("\n")
	}
	f.Close()
	err = os.WriteFile(path, []byte(stb.String()), 0555)
	check(err)
	if state == doNothing {
		fmt.Println("successfully patched the entrypoint")
	} else {
		fmt.Println("[WARN] unable to patch the entrypoint")
	}
	// must add this
	//    sget-object v0, Ldbg/roid/Init;->INSTANCE:Ldbg/roid/Init;

	//    invoke-virtual {v0}, Ldbg/roid/Init;->init()V

	//after
	//
	//.method protected onCreate(Landroid/os/Bundle;)V
	//.locals 9
	//.param p1, "savedInstanceState"    # Landroid/os/Bundle;

}

func Manif() {
	//	path := tmp_dir + "/apk/fr/app-debug/AndroidManifest.xml"
	//	manif, err := roidmanif.Parse(path)
	//	check(err)
	//	file, _ := xml.MarshalIndent(manif, "", " ")

	// _ = os.WriteFile("notes1.xml", file, 0555)
}
func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}
